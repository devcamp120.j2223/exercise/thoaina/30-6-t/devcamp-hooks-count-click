import { useEffect, useState } from "react";

function Count() {
    const [count, setCount] = useState(0);

    const increaseCount = () => {
        // console.log("increase count: " + count);
        setCount(count + 1);
    }

    useEffect(() => {
        console.log("increase count: " + count);
        document.title = `You click me ${count} times`
    });

    return (
        <div>
            <p>You click me {count} times</p>
            <button onClick={increaseCount}>Click me</button>
        </div>
    )
}

export default Count;